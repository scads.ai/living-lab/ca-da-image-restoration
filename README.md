# content-aware depth-adaptive image-restoration
This image restoration pipeline brings fine-grained user control to state-of-the-art generative models; in a deterministic and explainable way.

![pipeline-overview](Thesis/Tutorial/process-flow.png)

## Setup the environment
The `requirements.txt` file contains all the modules required to execute the pipeline using any model provided in the Codes directory.

Default configuration assumes that a GPU, Python version 3.8.6 and CUDA 11.7.0 are available. Same configuration should work smoothly in any other compatible environment.

## Testing the pipeline
Open the [Final_Pipeline](Final_Pipeline.ipynb) Jupyter Notebook to test the pipeline.

### User-editable configuration file
The integers inside the list are the stages of the pipeline in which the corresponding model can be used. Negative signs make a model the default choice for that stage.
zero (0) can be used to skip a model altogether. 

![pipeline-overview](Thesis/Tutorial/pipeline-configuration.png)

Created pipeline:

![pipeline-overview](Thesis/Tutorial/how-to-introduction-1.gif)


### Execution of the pipeline
Users can use the `Automate` button to execute the first 4 stages of the pipeline after making their preferred choice of models for each stage. If unsatisfied with the execution of a step, the user can come back and resume the execution from that particular step using the separate `Process` button from each Tab. 

Before re-executing with another image, pipeline should be reset using the `Reset` button

![pipeline-execution](Thesis/Tutorial/how-to-introduction-2.gif)

#### Input image
The image to work on should be present inside the `/Thesis/Images` directory and should be selected from the main dropdown before the Tabs. 

![zebra_test2](https://github.com/tomrv22/content-depth-aware-restoration/assets/105001497/f7ff4819-4075-4a3c-9f85-63cba70237f1)

#### Custom layout and Fine-tuning

Fine Tuning stage allows the user to make prompt-based edits to individual objects or even background. The `Image Editor` Tab permits layout customization using a simple dragging functionality. The link in this Tab takes the user to a web-based application running on another port of the machine. Here the user can reposition the objects in x, y, and z directions and even remove objects if they want to.

![pipeline-editor](Thesis/Tutorial/how-to-introduction-3.gif)

The `Default positions` button in the app allows one to maintain the original position of the objects from the parent image. Double-clicking any object in this editor sends the object to the background. Right-clicking allows the user to delete the objects. Clicking the `Save Canvas` button exits the app and saves the image which can be displayed in the `Restored Image` stage in the pipeline. 


## The Full Demo
The following video shows in detail each step that is explained above

![](Thesis/Tutorial/how-to-introduction.mp4)
